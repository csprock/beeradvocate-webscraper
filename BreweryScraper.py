import requests
from bs4 import BeautifulSoup as bs
import re
import time

#url = 'https://www.beeradvocate.com/place/list/?start=0&c_id=US&s_id=MN&brewery=Y&sort=name'
#
#results = requests.get(url)
#page = bs(results.text, 'html.parser')
#
#re_exp = '\/beer\/profile\/\d+\/'
#temp = page.find_all('a', {'href':re.compile(re_exp)})
#
#temp[0].parent.next_sibling.next_sibling.next_sibling.next_sibling



def get_brewery_links(page):
    re_exp = '\/beer\/profile\/\d+\/'
    return page.find_all('a', {'href':re.compile(re_exp)})



def check_brewery_links(a, min_ratings):
    
    status = False
    
    number_of_ratings = a.parent.next_sibling.next_sibling.string
    try:
        number_of_ratings = int(number_of_ratings)
        if number_of_ratings > min_ratings:
            status = True
    except:
        status = False
    
    return status


def parse_brewery_link(a):
    return {'name': a.string, 'link': a.attrs['href']}



def get_number_of_breweries(page):
    
    re_exp = 'Results:\s(\d+)\sto\s(\d+)\s\(out\sof\s(\d+)\):'
    p = re.compile(re_exp)
    temp = page.find('span', text = p).string
    m = p.match(temp)
    return int(m.group(1)), int(m.group(2)), int(m.group(3))




def scrape_breweries(base_url, filter_ratings = True, min_ratings = 10):
    
    brewery_list = list()
    
    results = requests.get(base_url.format(0))
    page = bs(results.text, 'html.parser')
    _, _, total = get_number_of_breweries(page)
    time.sleep(1)
    
    end = 0
    while end < total:    
        
        results = requests.get(base_url.format(end))
        page = bs(results.text, 'html.parser')
        
        start, end, _ = get_number_of_breweries(page)
        
        link_list = get_brewery_links(page)
        
        if filter_ratings:
            link_list = list(filter(lambda a: check_brewery_links(a, min_ratings), link_list))
        
        brewery_list.extend(list(map(parse_brewery_link, link_list)))
        
        time.sleep(1)
        print("%s / %s" % (end, total))
        
    return brewery_list





#
#base_url = 'https://www.beeradvocate.com/place/list/?start={}&c_id=US&s_id=MN&brewery=Y&sort=name'
#
#brewery_list = list()
#
#end = 0
#while end < total:    
#    
#    results = requests.get(base_url.format(end))
#    page = bs(results.text, 'html.parser')
#    
#    start, end, _ = get_number_of_breweries(page)
#    
#    link_list = get_brewery_links(page)
#    link_list = list(filter(lambda a: check_brewery_links(a, min_ratings), link_list))
#    
#    brewery_list.extend(list(map(parse_brewery_link, link_list)))
    
    
#    
#    time.sleep(1)
#    print("%s / %s" % (end, total))
    
    





















