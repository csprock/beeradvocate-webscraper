import requests
from bs4 import BeautifulSoup as bs
import re
import time

#def string_between_br(tag):
#    
#    if tag.previous_sibling == None or tag.next_sibling == None:
#        return False
#    else:
#        return tag.next_sibling.name == 'br' and tag.previous_sibling.name == 'br' and isinstance(tag, NavigableString)

def parse_ratings(ratings_string):
    '''
    Parse rating string. Example string: 'look: 3.25 | smell: 3.75 | taste: 3.5 | feel: 3.5 | overall: 3.5'
    '''
    ratings = dict()
    
    for r in ratings_string.split('|'):
        temp = r.split(':')
        ratings[temp[0].strip()] = float(temp[1].strip())
        
    return ratings
        

def number_of_ratings(page):
    '''
    Extract number of ratings from the page. 
    '''
    
    regex = 'Ratings:\s\d+(?:,?\d+)?'
    
    temp = page.find(string = re.compile(regex))
    return int(temp.split(':')[1].strip().replace(',',''))



def parse_review(review):
    '''
    Takes a div element corresponding to a review (from extract_reviews()) and parses for 
    the overall rating, username and ratings. If no username is found, status is returned False
    and review will be ignored in later-stage filtering. The beer attribute ratings are parsed with 
    the parse_ratings() function.
    '''
    status = True
    
    # Get overall score
    overall_rating = float(review.find('span', {'class':'BAscore_norm'}).contents[0])
    
    # Get username
    try:
        user_id = review.find('a', {'class':'username'}).string
    except IndexError:
        status = False
    
    # Get ratings and parse with parse_ratings()
    try:
        re_exp = '(:?\w+:\s(?:\d*\.?\d+)\s\|\s)+\w+:\s\d*\.?\d+'
        ratings = parse_ratings(review('span', text = re.compile(re_exp))[0].contents[0])
    except IndexError:
        ratings = None
    
    
#    try:
#        ratings = review('span', text = re.compile(re_exp))[0]
#        ratings_text = str(ratings.next_sibling.next_sibling.next_sibling.string)
#        ratings = parse_ratings(review('span', text = re.compile(re_exp))[0].contents[0])
#    except:
#        
#        #anchor = review.find('span', {'class':'rAvg_norm'})
#        #ratings_text = anchor.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.string
#        ratings = None
#        ratings_text = list(filter(string_between_br, review.contents))
#        if len(ratings_text) == 0:
#            ratings_text = None
#        elif len(ratings_text) == 1:
#            ratings_text = ratings_text[0]
#        else: # detect edge cases
#            raise ValueError("Warning! Found more than one text pattern")
        
    return status, {'overall_rating': overall_rating, 'ratings': ratings, 'user': user_id}

def extract_reviews(page_soup):
    '''
    Extract all div elements corresponding to reviews.
    '''
    return page_soup.find_all("div", {'id': 'rating_fullview_content_2'})
        

def process_reviews(review_list):
    '''
    Takes list of reviews from extract_reviews() function and applies the parse_review() function to each.
    Reviews with status:False are ignored. 
    '''
    
    processed_reviews = list()
    
    for r in review_list:
        status, parsed_review = parse_review(r)
        if status:
            processed_reviews.append(parsed_review)
        
    return processed_reviews


def get_beer_page(beer_url, start):
    '''
    Takes the base URL of the beer profile page and a start index corresponding to the first review
    the page will display and returns parsed BeautifulSoup object.
    '''
    response = requests.get(beer_url + '?view=beer&sort=&start={}'.format(start))
    if response.status_code != 200:
        raise ValueError("Status code: %s" + response.status_Code)
        
    return bs(response.text, 'html.parser')

    


def scrape_reviews(beer_page_url, delay = 1, verbose = True):
    
    # get beer profile page and parse with BS.
    response = requests.get(beer_page_url)
    beer_page = bs(response.text, 'html.parser')
    
    # get number of ratings
    N = number_of_ratings(beer_page)   
    
    beer_reviews = list()
    p, j = 0, 0  # initialize counters, p = page, j = index of rating to pass to get_beer_page()
    
    # While the length of the beer_reviews list is less than the total number of reviews, do:
    #   call get_beer_page() on next beer with index set to current length of beer_reviews list
    #   call extract_reviews() on returned BS object
    #   call process_reviews() and add to beer_review list
    #   update counters for next iteration
    #   sleep
    
    while len(beer_reviews) < N:
        beer_page = get_beer_page(beer_page_url, j)
        reviews = extract_reviews(beer_page)
        beer_reviews.extend(process_reviews(reviews))
        j += len(reviews)
        p += 1
        if verbose: print("Page: %s, Reviews Scraped: %s / %s" % (p, len(beer_reviews), N))
        time.sleep(delay)
        
    return beer_reviews
    
#
#response = requests.get('https://www.beeradvocate.com/beer/profile/467/215090/')
##
#soup = bs(response.text, 'html.parser')
#N = number_of_ratings(soup)
#
#beer_reviews = list()
#
#p = 0
#j = 0
#while len(beer_reviews) < N:
#    beer_page = get_beer_page('https://www.beeradvocate.com/beer/profile/467/215090/', j)
#    reviews = extract_reviews(beer_page)
#    beer_reviews.extend(process_reviews(reviews))
#    j += len(reviews)
#    p += 1
#    print("Page: %s, Reviews Scraped: %s / %s" % (p, len(beer_reviews), N))
#    time.sleep(2)
#    
#
#reviews = scrape_reviews('https://www.beeradvocate.com' + '/beer/profile/31383/105092/')
#
#source = requests.get('https://www.beeradvocate.com/beer/profile/467/')
#
#
#soup = bs(source.text, 'html.parser')
#
#soup.find_all({'valign':'top'})
#
#
#
#reviews = soup.find_all("div", {'id': 'rating_fullview_content_2'})
#
#sample_review = reviews[1]
#temp = get_beer_page('https://www.beeradvocate.com' + '/beer/profile/31383/105092/', 105)
#
#revs = extract_reviews(temp)
#revs2 = process_reviews(revs)
#
#for r in revs:
#    print(parse_review(r))
#
#
#revs2 = process_reviews(revs)
#
#
#test_rev = revs[13]
#
#len(test_rev.find_all('br', recursive = False))





