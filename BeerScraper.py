from selenium import webdriver
from bs4 import BeautifulSoup as bs
import re



class SeleniumDriver:

    def __init__(self, chrome_path = '/Applications/chromedriver'):
        self.driver = webdriver.Chrome(chrome_path)
        
    def get_rendered_page(self, url):
        self.driver.get(url)
        return self.driver.execute_script("return document.getElementsByTagName('html')[0].innerHTML")
    
    
    def close(self):
        self.driver.close()
        print("Driver closed.")
        
def scrape_beers(url):
    
    print("Starting Selenium Webdriver...")
    Driver = SeleniumDriver()
    print("Getting webpage...")
    page = Driver.get_rendered_page(url)
    Driver.close()
    
    page = bs(page, 'html.parser')
    rows = page.find_all('tr', {'role':'row'})
    rows = rows[1:]   # remove table header
    
    rows = filter(lambda x: number_of_reviews(x) > 0, rows)   # filter by number of ratings
    rows = filter(lambda x: x != None, map(get_info, rows))
    return list(rows)
    

def number_of_reviews(page):
    children = [c for c in page.children]
    n_reviews = children[3].string.replace(',', '')
    return int(n_reviews)

def parse_link(a):
    return {'beer': a.string, 'link': a.attrs['href']}

def get_link(page):
    re_exp = '\/beer\/profile\/\d+\/\d+\/'
    return page.find('a', {'href':re.compile(re_exp)})
    

def get_style(page):
    children = [c for c in page.children]
    return str(children[1].string)

def get_info(page):
    
    link = get_link(page)
    if link != None:
        link_and_name = parse_link(link)
    else:
        return None
    
    style = get_style(page)
    link_and_name.update({'style':style})
    
    return link_and_name



#
#rows = scrape_beers('https://www.beeradvocate.com/beer/profile/43126/')
#rows = rows[1:]
#rows = filter(lambda x: number_of_reviews(x) > 0, rows)
#rows = filter(lambda x: x != None, map(get_link, rows))
#rows = list(map(parse_link, rows))
#
#
#list(filter(lambda x: x != None, map(get_link, filter(lambda x: number_of_reviews(x) > 0, rows))))
#
#for r in rows:
#    print(number_of_reviews(r))
#
#rows = list(filter(lambda x: number_of_reviews(x) > 0, rows))
#rows = list(map(parse_link, rows))


